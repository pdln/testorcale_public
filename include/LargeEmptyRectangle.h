#ifndef LARGEEMPTYRECTANGLE_H
#define LARGEEMPTYRECTANGLE_H

#include <set>
#include <list>
#include <memory>

#include "GeoCoordinates.h"
#include "Rectangle.h"
#include "SourceData.h"
#include "DataMatrix.h"

/**
 * @brief The LargeEmptyRectangle class
 */
class LargeEmptyRectangle
{
public:
    using ShortRect = Rectangle<short>;

private:

    /**
     * @brief The Rect_x_comparator struct
     */
    struct Rect_x_comparator
    {
        bool operator()(const ShortRect& r1, const ShortRect& r2) const noexcept
        {
            return r1.x < r2.x;
        }
    };

    /**
     * @brief The Rect_y_comparator struct
     */
    struct Rect_y_comparator
    {
        bool operator()(const ShortRect& r1, const ShortRect& r2) const noexcept
        {
            return r1.y < r2.y;
        }
    };

public:
    struct Result
    {
        ShortRect                                    max;   /// max rect size
//        std::multiset<ShortRect, Rect_x_comparator>  top;
//        std::multiset<ShortRect, Rect_x_comparator>  bottom;
//        std::multiset<ShortRect, Rect_y_comparator>  left;
//        std::multiset<ShortRect, Rect_y_comparator>  right;
    };
    typedef std::shared_ptr<Result> ResultPtr;
public:
    LargeEmptyRectangle() = default;
    ShortRect exec(SourceData<GeoCoordinates>& data);
    ResultPtr exec(DataMatrix::DataPtr data);
};

#endif // LARGEEMPTYRECTANGLE_H

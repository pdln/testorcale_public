#ifndef UUID_H
#define UUID_H

#include <random>
#include <sstream>

namespace Uuid
{
    /**
     * @brief generate_uuid_v4
     * @return uuid v4 string
     */
    static std::string generate_uuid_v4()
    {
        static std::random_device              randDevice;
        static std::mt19937                    gen(randDevice());
        static std::uniform_int_distribution<> range1(0, 15);
        static std::uniform_int_distribution<> range2(8, 11);

        std::stringstream ss;
        int i;
        ss << std::hex;
        for (i = 0; i < 8; i++)
            ss << range1(gen);

        ss << "-";
        for (i = 0; i < 4; i++)
            ss << range1(gen);

        ss << "-4";
        for (i = 0; i < 3; i++)
            ss << range1(gen);

        ss << "-";
        ss << range2(gen);

        for (i = 0; i < 3; i++)
            ss << range1(gen);

        ss << "-";
        for (i = 0; i < 12; i++)
            ss << range1(gen);

        return ss.str();
    }
}

#endif // UUID_H

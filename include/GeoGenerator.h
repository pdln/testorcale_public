#ifndef GEOGENERATOR_H
#define GEOGENERATOR_H

#include "GeoRandomizer.h"

/**
 * @brief The GeoGenerator class
 * generate n geo coordinates and store it into file
 */
class GeoGenerator
{
public:
    GeoGenerator() = default;
    ~GeoGenerator() = default;

    /**
     * @brief generate n geo coordinates and store it into file
     * @param dir directory to store data
     * @param quantity
     * @param partitionSize bufer size
     * @return file name
     */
    std::string generate(const std::string& dir,
                         size_t quantity,
                         size_t partitionSize = 1000000);

private:
    GeoRandomizer m_geoRandomizer;
};

#endif // GEOGENERATOR_H

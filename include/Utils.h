#ifndef UTILS_H
#define UTILS_H

#include <cmath>
#include <algorithm>

namespace Utils
{
   /**
     * @brief fuzzyCompare double comparation function
     * @param p1 first value
     * @param p2 second value
     * @return thrue if values are same
     */
    static inline bool fuzzyCompare(double p1, double p2)
    {
        return (std::abs(p1 - p2) * 1000000000000. <= std::min(std::abs(p1), std::abs(p2)));
    }

    /**
     * if vavue compares less than lower, returns lower
     * otherwise if heigher compares less than value, returns heigher
     * otherwise returns value
     *
     * https://en.cppreference.com/w/cpp/algorithm/clamp
     */
    template <typename T>
    T clamp(const T& value, const T& lower, const T& heigher)
    {
        return (value < lower) ? lower : (heigher < value) ? heigher : value;
    }
}

#endif // UTILS_H

#ifndef NONCOPYABLE_H
#define NONCOPYABLE_H

/**
 * @brief The NonCopyable class
 */
class NonCopyable
{
public:
    NonCopyable() = default;
    NonCopyable(const NonCopyable&) = delete;
    NonCopyable(const NonCopyable&&) = delete;
    NonCopyable& operator=(const NonCopyable&) = delete;
    NonCopyable& operator=(NonCopyable&&) = delete;

protected:
    virtual ~NonCopyable() = default;
};

#endif // NONCOPYABLE_H

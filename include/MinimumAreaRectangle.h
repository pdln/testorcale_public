#ifndef MINIMUMAREARECTANGLE_H
#define MINIMUMAREARECTANGLE_H

#include "GeoCoordinates.h"
#include "Rectangle.h"
#include "SourceData.h"
/**
 * @brief The MinimumAreaRectangle class
 */
class MinimumAreaRectangle
{
public:
    MinimumAreaRectangle();
    Rectangle<short> exec(SourceData<GeoCoordinates>& data);
};

#endif // MINIMUMAREARECTANGLE_H

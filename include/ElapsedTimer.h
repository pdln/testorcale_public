#ifndef ELAPSEDTIMER_H
#define ELAPSEDTIMER_H

#include <chrono>

#include "NonCopyable.h"

/**
 * @brief The ElapsedTimer class
 * elapsed timer
 */
class ElapsedTimer : public NonCopyable
{
public:
    ElapsedTimer()
        : m_start(clock_t::now())
    {
        ///
    }

    /**
     * @brief reset time
     */
    void reset()
    {
        m_start = clock_t::now();
    }

    /**
     * @brief elapsed
     * @return elapsed time
     */
    double elapsed() const
    {
        return std::chrono::duration_cast<duration_t>(clock_t::now() - m_start).count();
    }

private:
    using clock_t = std::chrono::high_resolution_clock;
    using duration_t = std::chrono::duration<double, std::ratio<1>>;

    std::chrono::time_point<clock_t> m_start;   /// start time
};

#endif // ELAPSEDTIMER_H

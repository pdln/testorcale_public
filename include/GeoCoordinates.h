#ifndef GEOCOORDINATES_H
#define GEOCOORDINATES_H

#include <ostream>

/**
 * @brief The GeoCoordinates class
 * Since the algorithm assumes an error of 0.1 degrees.
 * It makes sense to save on memory and processing waste.
 * Instead of double we use short
 */
class GeoCoordinates
{
public:
    GeoCoordinates();
    GeoCoordinates(short latitude, short longitude);
    GeoCoordinates(const GeoCoordinates& other);
    GeoCoordinates(GeoCoordinates&& other);
    ~GeoCoordinates() = default;

    GeoCoordinates& operator=(const GeoCoordinates& other);
    GeoCoordinates& operator=(GeoCoordinates&& other);
    bool operator==(const GeoCoordinates& other);
    bool operator<=(const GeoCoordinates& other);

    std::ostream& write(std::ostream& os) const;
    std::ifstream& read(std::ifstream& in);

    /**
     * @brief latitude
     * @return latitude
     */
    short latitude() const;

    /**
     * @brief longitude
     * @return longitude
     */
    short longitude() const;

    /**
     * @brief setLatitude set latitude. without control!
     * @param latitude
     */
    void setLatitude(const short latitude);

    /**
     * @brief setLongitude set longitude. without control!
     * @param longitude
     */
    void setLongitude(const short longitude);

private:
    short m_latitude;       /// latitude
    short m_longitude;      /// longitude
};

inline std::ostream& operator<<(std::ostream& os, const GeoCoordinates& point)
{
    return point.write(os);
}

inline std::ifstream& operator>>(std::ifstream& in, GeoCoordinates& point)
{
    return point.read(in);
}


#endif // GEOCOORDINATES_H

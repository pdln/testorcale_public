#ifndef ALGORITHM_H
#define ALGORITHM_H

#include "GeoCoordinates.h"
#include "Rectangle.h"
#include "SourceData.h"

/**
 * @brief The Algorithm class
 * strategy pattern implementation for executing algorithms
 */
template <class Implementation>
class Algorithm: public Implementation
{
public:
    Algorithm(const std::string& filePath)
        : m_data(filePath)
    {
        ///
    }

    Algorithm(std::string&& filePath)
        : m_data(std::move(filePath))
    {
        ///
    }

    /**
     * @brief exec
     * @return Rectangle result
     */
    Rectangle<short> exec()
    {
        return Implementation::exec(m_data);
    }

private:
    SourceData<GeoCoordinates> m_data;  /// source data
};

#endif // ALGORITHM_H

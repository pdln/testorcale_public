#ifndef MATRIX_H
#define MATRIX_H

#include <valarray>

/**
 * @brief The Matrix class
 * class for working with matrix
 */
template<typename T>
class Matrix
{
 public:
    /**
     * @brief The RowWrapper class
     *  Row wrapper for getting data from matix [][]
     */
    class RowWrapper
    {
    public:
        RowWrapper(std::valarray<T>& data, size_t rows, size_t cols)
            : m_row(rows)
            , m_col(cols)
            , m_data(data)
        {
            ///
        }

        T& operator[](const int col)
        {
            return m_data[m_row*m_col + col];
        }

    private:
        size_t              m_row; /// row
        size_t              m_col; /// column
        std::valarray<T>&   m_data; /// Reference to data array.
    };

    class RowWrapperConst
    {
    public:
        RowWrapperConst(const std::valarray<T>& data, size_t rows, size_t cols)
            : m_row(rows)
            , m_col(cols)
            , m_data(data)
        {
            ///
        }

        T operator[](const int col) const
        {
            return m_data[m_row*m_col + col];
        }

    private:
        size_t                  m_row; /// row
        size_t                  m_col; /// column
        const std::valarray<T>& m_data; /// Reference to data array.
    };


    Matrix(size_t row, size_t col)
        : m_rows(row)
        , m_cols(col)
        , m_data(row*col)
    {
        ///
    }

    RowWrapper operator[](const int row)
    {
        return RowWrapper(m_data, row, m_cols);
    }

    RowWrapperConst operator[](const int row) const
    {
        return RowWrapperConst(m_data, row, m_cols);
    }

    /**
     * @brief rows
     * @return rows count
     */
    size_t rows() const
    {
        return m_rows;
    }

    /**
     * @brief cols
     * @return columns count
     */
    size_t cols() const
    {
        return m_cols;
    }


private:
    size_t              m_rows; /// rows count
    size_t              m_cols; /// columns count
    std::valarray<T>    m_data; /// data array. valarray supported math operation for each element
};

#endif // MATRIX_H

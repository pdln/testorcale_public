#ifndef DATAMATRIX_H
#define DATAMATRIX_H

#include <memory>

#include "NonCopyable.h"

template<typename T>
class Matrix;

/**
 * @brief The DataMatrix class
 * generate n geo coordinates and store data into matrix
 */
class DataMatrix : public NonCopyable
{
public:
    using UCharMatrix = Matrix<u_char>;
    typedef std::shared_ptr<UCharMatrix> DataPtr;

public:
    DataMatrix();
    ~DataMatrix() = default;

    /**
     * @brief data
     * @return get matrix pointer
     */
    DataPtr data() const;

    /**
     * @brief generate geo coordinates and fill matrix
     * @param quantitiy
     */
    void generate(const size_t quantitiy);

private:
    DataPtr m_data; /// matrix geo coordinates
};

#endif // DATAMATRIX_H

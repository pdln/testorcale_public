#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <ostream>
#include <type_traits>

/**
 * @brief The Rectangle struct
 */
template <typename T>
struct Rectangle
{
    static_assert(std::is_integral<T>::value, "Integral required.");

    Rectangle()
        : x(0)
        , y(0)
        , width(0)
        , height(0)
    {
        ///
    }

    Rectangle(T x, T y, T w, T h)
        : x(x)
        , y(y)
        , width(w)
        , height(h)
    {
        ///
    }

    ~Rectangle() = default;


//    Rectangle& operator+=(const Rectangle& other);
    bool operator==(const Rectangle& other) const
    {
        return x == other.x && y == other.y && area() == other.area();
    }

    bool operator<(const Rectangle& other) const
    {
        if(x == other.x)
            return y < other.y;
        return x < other.x;
    }

    std::ostream& write(std::ostream& os) const
    {
        return os << "[(" << x << "," << y - (height - 1) << ")(" << x + width - 1 << "," << y <<")]";
    }

    /**
     * @brief area
     * @return rectangle area
     */
    T area() const
    {
        return width * height;
    }

    T x;          // bottom-left x
    T y;          // bottom-left y
    T width;      // width
    T height;     // height
};

template <typename T>
inline std::ostream& operator<<(std::ostream& os, const Rectangle<T>& rect)
{
    return rect.write(os);
}

#endif // RECTANGLE_H

#ifndef DATAGENERATOR_H
#define DATAGENERATOR_H

#include <queue>
#include <future>

#include "ElapsedTimer.h"

class ThreadPool;
/**
 * @brief The DataGenerator class
 * generate n geo coordinates and store data into sorted file
 */
class DataGenerator
{
public:
    DataGenerator(ThreadPool& pool, const std::string& dir);
    ~DataGenerator() = default;
    std::string exec(size_t quantity = 1000000000, size_t partitionSize = 1000000);

private:
    /**
     * @brief waitingData waiting processing data
     * @return file name
     */
    std::string waitingData();

    /**
     * @brief makeMerge merge data
     * @return file name
     */
    std::string makeMerge();

private:
    ElapsedTimer                         m_timer;       /// elapsed timer
    std::string                          m_dir;         /// directory to store new file
    ThreadPool&                          m_pool;        /// thread pool reference
    std::queue<std::string>              m_results;     /// results queue
    std::queue<std::future<std::string>> m_futures;     /// futures queue
};

#endif // DATAGENERATOR_H

#ifndef BLOCKINGQUEUE_H
#define BLOCKINGQUEUE_H

#include <mutex>
#include <condition_variable>
#include <queue>

/**
 * @brief The BlockingQueue class
 * queue that additionally supports operations that wait for the queue
 * to become non-empty when retrieving an element
 */
template<typename T>
class BlockingQueue
{
    typedef std::lock_guard<std::mutex> lock;
    typedef std::unique_lock<std::mutex> ulock;
public:
    BlockingQueue() = default;
    ~BlockingQueue() = default;

    /**
     * @brief front
     * @return reference on first data
     */
    T& front()
    {
        ulock ulock(m_mutex);
        // lock and wait data
        m_condition.wait(ulock, [&](){
            return !m_data.empty();
        });

        T& data = m_data.front();
        return data;
    }

    /**
     * @brief back
     * @return reference on last data
     */
    T& back()
    {
        ulock ulock(m_mutex);
        // lock and wait data
        m_condition.wait(ulock, [&](){
            return !m_data.empty();
        });

        T& data = m_data.back();
        return data;
    }

    /**
     * @brief empty
     * @return true if queue is empty
     */
    bool empty()
    {
        return m_data.empty();
    }

    /**
     * @brief size
     * @return queue size
     */
    size_t size()
    {
        return m_data.size();
    }

    /**
     * @brief push copy into queue
     * @param data
     */
    void push(const T& data)
    {
        {
            lock lock(m_mutex);
            m_data.push(data);
        }
        m_condition.notify_one();
    }

    /**
     * @brief push move data into queue
     * @param data
     */
    void push(T&& data)
    {
        {
            lock lock(m_mutex);
            m_data.emplace(data);
        }
        m_condition.notify_one();
    }

    /**
     * @brief pop removes the first element
     */
    void pop()
    {
        lock lock(m_mutex);
        if(!m_data.empty())
            m_data.pop();
    }

private:
    std::mutex              m_mutex;        /// mutex for safe work with queue
    std::condition_variable m_condition;    /// condition for notification of adding a new data
    std::queue<T>           m_data;         /// queue data
};

#endif // BLOCKINGQUEUE_H

#ifndef THREADPOOL_H
#define THREADPOOL_H

#include <thread>
#include <functional>
#include <future>
#include <type_traits>

#include "BlockingQueue.h"
#include "Log.h"
#include "NonCopyable.h"

/**
 * @brief The ThreadPool class
 * Thread pool with task pool based on blocking queue.
 * Able to work with any type of tasks.
 */
class ThreadPool : public NonCopyable
{
    using Task = std::function<void()>;
    using Threads = std::vector<std::thread>;

public:
    explicit ThreadPool(uint numThreads = std::thread::hardware_concurrency())
        : m_terminate(false)
        , m_threads()
        , m_tasks()
    {
        /// thread worker
        auto worker = [&](){
            try
            {
                for(;;)
                {
                    Task task;

                    {
                        std::unique_lock<std::mutex> lock(m_mutex);
                        /// waiting data if task queue is empty
                        m_condition.wait(lock, [this](){return m_terminate || !m_tasks.empty();});
                        if(m_terminate && m_tasks.empty())
                            return;

                        task = m_tasks.front();
                        m_tasks.pop();
                    }

                    task();
                }
            }
            catch(std::runtime_error& e)
            {
                ERROR(e.what())
            }
        };

        for(uint i = 0; i < numThreads; ++i)
            m_threads.emplace_back(worker);
    }

    ~ThreadPool()
    {
        m_terminate = true;

        for(auto& thread: m_threads)
            thread.join();
    }

    /**
     * @brief threads
     * @return pool size
     */
    uint threads() const
    {
        return m_threads.size();
    }

    /**
     *  enqueue job to task queue
     *  variadic templates gives us the ability to accept a task with any number of parameters
     */
    template<typename F, typename... Args>
    auto enqueue(F&& f, Args&&... args)->std::future<typename std::result_of<F(Args...)>::type>
    {
        // wrapping our function in packaged_task for call asynchronously
        // https://en.cppreference.com/w/cpp/thread/packaged_task
        using return_type = typename std::result_of<F(Args...)>::type;
        using task_type = std::packaged_task<return_type()>;

        auto task = std::make_shared<task_type>(f);
        auto result = task->get_future();

        {
            std::unique_lock<std::mutex> lock(m_mutex);
            m_tasks.emplace([task, &args...](){
                (*task)(std::forward<Args>(args)...);
            });
        }

        m_condition.notify_one();
        return result;
    }

private:
    bool                    m_terminate;    /// terminate flag
    std::mutex              m_mutex;        /// mutex for safe work with tasks queue
    Threads                 m_threads;      /// threads pool
    std::queue<Task>        m_tasks;        /// tasks queue
    std::condition_variable m_condition;    /// condition for notification of adding a new task
//    BlockingQueue<Task> m_tasks;

};

#endif // THREADPOOL_H

#ifndef APPSETTINGS_H
#define APPSETTINGS_H

#include <string>

/**
 * @brief The AppSettings class
 * encapsulates application settings
 */
class AppSettings
{
public:
    /**
     * @brief The Flags enum
     * supported app operations
     */
    enum class Flags : char
    {
        Threads         = 0x01, /// threads count flag
        Generate        = 0x02, /// generate data flag
        Partition       = 0x04, /// settings partitions size for generate data
        InputFile       = 0x08, /// get data from input file
        LerAlgorithm    = 0x10, /// Ler algorithm enable
        MarAlgorithm    = 0x20, /// Mfr algorithm enable
        Store           = 0x40  /// store result
    };

    AppSettings(int argc, char *argv[]);
    ~AppSettings() = default;

    /**
     * @brief operations
     * @return current executable operations
     */
    Flags operations() const;

    /**
     * @brief testFlag
     * @param flag operation to check
     * @return true if operation is enabled in current session
     */
    bool testFlag(const Flags flag) const;

    /**
     * @brief threads
     * @return current session thread pool size
     */
    uint threads() const;

    /**
     * @brief generate
     * @return amount of data to be generated
     */
    size_t generate() const;

    /**
     * @brief partition
     * @return buffer size for data generation
     */
    size_t partition() const;

    /**
     * @brief file
     * @return input file path
     */
    const std::string& file() const;

private:
    Flags       m_operations;       /// current executable operations
    uint        m_threads;          /// current session thread pool size
    size_t      m_generate;         /// amount of data to be generated
    size_t      m_partition;        /// buffer size for data generation
    std::string m_file;             /// input file path

};

inline AppSettings::Flags operator|(AppSettings::Flags first, AppSettings::Flags second)
{
    return static_cast<AppSettings::Flags>(static_cast<char>(first)|static_cast<char>(second));
}

inline AppSettings::Flags operator&(AppSettings::Flags first, AppSettings::Flags second)
{
    return static_cast<AppSettings::Flags>(static_cast<char>(first)&static_cast<char>(second));
}

#endif // APPSETTINGS_H

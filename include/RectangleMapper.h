#ifndef RECTANGLEMAPPER_H
#define RECTANGLEMAPPER_H

#include <utility>

#include "GeoCoordinates.h"
#include "GeoRandomizer.h"
#include "Rectangle.h"

/**
 * @brief The RectangleMapper class map rectangle
 */
template<typename T>
class RectangleMapper
{
public:
    RectangleMapper(Rectangle<T> rect)
        : m_rect(rect)
    {

    }

    /**
     * @brief toGeoCoordinates
     * @return pair of geo coordinates
     */
    std::pair<GeoCoordinates, GeoCoordinates> toGeoCoordinates() const
    {
        return std::make_pair(GeoCoordinates((m_rect.x - GeoRandomizer::max_latitude),
                                             (m_rect.y - (m_rect.height - 1) - GeoRandomizer::max_longitude)),
                              GeoCoordinates((m_rect.x + m_rect.width - 1 - GeoRandomizer::max_latitude),
                                             (m_rect.y - GeoRandomizer::max_longitude)));
    }

private:
    Rectangle<T> m_rect;
};

#endif // RECTANGLEMAPPER_H

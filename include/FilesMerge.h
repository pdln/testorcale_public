#ifndef FILESMERGE_H
#define FILESMERGE_H

#include <string>

/**
 * @brief The FilesMerge class
 * merge and sort 2 files and store into new
 */
class FilesMerge
{
public:
    FilesMerge() = default;
    ~FilesMerge() = default;

    /**
     * @brief merge
     * @param dir directory to store sorting data
     * @param first file
     * @param second file
     * @return union sorted file name
     */
    std::string merge(const std::string& dir,
                      const std::string& first,
                      const std::string& second);
};

#endif // FILESMERGE_H

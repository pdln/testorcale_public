#ifndef GEORANDOMIZER_H
#define GEORANDOMIZER_H

#include <random>

#include "NonCopyable.h"

class GeoCoordinates;
/**
 * @brief The GeoRandomizer class
 * helps get valid random geo coordinate
 */
class GeoRandomizer : public NonCopyable
{
public:
    static const short max_latitude;
    static const short min_latitude;
    static const short max_longitude;
    static const short min_longitude;
public:
    GeoRandomizer();
    ~GeoRandomizer() = default;

    /**
     * @brief generate random geo coordinates in ranges m_latitude_range, m_longitude_range
     * @return GeoCoordinates
     */
    GeoCoordinates generate();

    /**
     * @brief latitudeRange returned latitude ranges as pair. first value is min, second value is max
     * @return std::pair<int, int>
     */
    std::pair<short, short> latitudeRange() const;

    /**
     * @brief longitudeRange returned longitude ranges as pair. first value is min, second value is max
     * @return std::pair<int, int>
     */
    std::pair<short, short> longitudeRange() const;

    void setLatitudeRange(const short min, const short max);
    void setLongitudeRange(const short min, const short max);

private:
    std::mt19937                    m_gen;              /// seed the generator
    std::uniform_int_distribution<> m_latitudeRange;    /// latitude range
    std::uniform_int_distribution<> m_longitudeRange;   /// longitude range
};

#endif // GEORANDOMIZER_H

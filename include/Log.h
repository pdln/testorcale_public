#ifndef LOG_H
#define LOG_H

#include <iostream>

#define ERROR(x) std::cerr<< "E: [" << __PRETTY_FUNCTION__ << " line:" << __LINE__ << "]: " <<  x << std::endl;
#define TRACE(x) std::cout<< "T: [" << __PRETTY_FUNCTION__ << " line:" << __LINE__ << "]: " <<  x << std::endl;

#endif // LOG_H

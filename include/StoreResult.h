#ifndef STORERESULT_H
#define STORERESULT_H

#include "Rectangle.h"
#include "DataMatrix.h"

/**
 * @brief The StoreResult class
 * store result rect into file
 */
class StoreResult
{
public:
    StoreResult() = default;
    ~StoreResult() = default;

    /**
     * @brief store
     * @param file file name
     * @param rect data to store
     */
    void store(const std::string& file, const Rectangle<short>& rect);
    void store(const std::string& file, const DataMatrix::DataPtr data);
};

#endif // STORERESULT_H

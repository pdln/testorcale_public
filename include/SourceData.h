#ifndef SOURCEDATA_H
#define SOURCEDATA_H

#include <cassert>
#include <fstream>
#include <string>

/**
 * @brief The SourceData class
 */
template <typename Data>
class SourceData
{
public:
    SourceData(const std::string& file)
    {
        m_ifs.open(file, std::ios::in | std::ios::binary | std::ios::ate);
        m_size = m_ifs.tellg() / sizeof(Data);
    }

    ~SourceData()
    {
        m_ifs.close();
    }

    /**
     * @brief operator []
     * @param index
     * @return get data by index
     */
    Data operator[](size_t index)
    {
        assert(index < m_size);
        Data data;
        m_ifs.seekg(index * sizeof(Data));
        if(m_ifs)
            m_ifs >> data;
        return data;
    }

    /**
     * @brief size
     * @return size data
     */
    size_t size() const
    {
        return m_size;
    }


private:
    size_t          m_size; /// data size
    std::ifstream   m_ifs;  /// input file stream
};
#endif // SOURCEDATA_H

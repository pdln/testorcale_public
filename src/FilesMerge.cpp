#include <fstream>

#include <GeoCoordinates.h>
#include <FilesMerge.h>
#include <Uuid.h>

std::string FilesMerge::merge(const std::string& dir, const std::string& first, const std::string& second)
{
    /// merge and sort data
    std::string sorted = dir + "/" +Uuid::generate_uuid_v4();
    std::ofstream resultStream(sorted, std::ios_base::binary | std::ios_base::app);

    std::ifstream firstStream(first, std::ios_base::binary);
    std::ifstream secondStream(second, std::ios_base::binary);

    firstStream >> std::noskipws;
    secondStream >> std::noskipws;

    GeoCoordinates val1, val2, tmp;
    if(firstStream >> tmp)
        val1 = tmp;
    if(secondStream >> tmp)
        val2 = tmp;

    while(firstStream && secondStream)
    {
        if (val1 <= val2)
        {
            resultStream << val1;
            firstStream >> val1;
        }
        else
        {
            resultStream << val2;
            secondStream >> val2;
        }
    }

    if(firstStream)
        resultStream<< val1 << firstStream.rdbuf();

    if(secondStream)
        resultStream << val2 << secondStream.rdbuf();

    firstStream.close();
    secondStream.close();

    std::remove(first.data());
    std::remove(second.data());

    return sorted;
}

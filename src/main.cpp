#include <sys/stat.h>

#include <Algorithm.h>
#include <AppSettings.h>
#include <DataGenerator.h>
#include <DataMatrix.h>
#include <ElapsedTimer.h>
#include <LargeEmptyRectangle.h>
#include <Log.h>
#include <MinimumAreaRectangle.h>
#include <RectangleMapper.h>
#include <StoreResult.h>
#include <ThreadPool.h>
#include <Uuid.h>

int main(int argc, char *argv[])
{
    /// check commands
    AppSettings settings(argc, argv);
    std::string filePath;

    /// create jobs pool
    ThreadPool threads(settings.threads());
    ElapsedTimer timer;
    /*
    /// generate data if we need
    if(settings.testFlag(AppSettings::Flags::Generate))
    {
        /// create temp dir
        auto dir = Uuid::generate_uuid_v4();
        if(mkdir(dir.c_str(), 0777))
        {
            ERROR("Error create temp directory.");
            return EXIT_FAILURE;
        }

        TRACE("Start generate data.");
        DataGenerator generator(threads, dir);
        filePath = generator.exec(settings.generate(), settings.partition());
        TRACE("Source data stored: " << filePath);
    }

    /// if we get input file, try to validate data
    if(settings.testFlag(AppSettings::Flags::InputFile))
    {
        TRACE("check: " << settings.file());
        filePath = settings.file();
    }
*/

    std::string result_dir;

    if(settings.testFlag(AppSettings::Flags::Store))
    {
        /// create temp dir
        result_dir = Uuid::generate_uuid_v4();
        if(mkdir(result_dir.c_str(), 0777))
        {
            ERROR("Error create temp directory.");
            return EXIT_FAILURE;
        }
    }

    DataMatrix dataMatrix;

    /// generate data
    if(settings.testFlag(AppSettings::Flags::Generate))
    {
        TRACE("Start generate " << settings.generate() << " coordinates");
        dataMatrix.generate(settings.generate());
        TRACE("Generate data finished " << timer.elapsed());

	timer.reset();

        if(settings.testFlag(AppSettings::Flags::Store))
        {
            TRACE("Start store geo coordinates")
            auto resultFile = result_dir + "/" + "GeoData";
            StoreResult().store(resultFile, dataMatrix.data());
            TRACE("Geo coordinates are stored: " << resultFile 
                  << " " << timer.elapsed());
        }
    }

    /// find maximum area rectangle
    if(settings.testFlag(AppSettings::Flags::MarAlgorithm))
    {
	timer.reset();
        auto result = Algorithm<MinimumAreaRectangle>(filePath).exec();
        TRACE("Minimun area rectangle: " << result << " " << timer.elapsed());
        /// store data into file
        if(settings.testFlag(AppSettings::Flags::Store))
        {
            auto resultFile = result_dir + "/" + "MAR_result";
            StoreResult().store(resultFile, result);
            TRACE("Minimun area rectangle result stored: " << resultFile
		  << " " << timer.elapsed());
        }
    }

    /// find large empty area rectangle
    if(settings.testFlag(AppSettings::Flags::LerAlgorithm))
    {
        timer.reset();
//        auto result = Algorithm<LargeEmptyRectangle>(filePath).exec();
        auto result = LargeEmptyRectangle().exec(dataMatrix.data());

        const auto geo = RectangleMapper<short>(result->max).toGeoCoordinates();
        TRACE("Large empty rectangle: "
              << "[(" << geo.first.latitude() / 100.
              << "," << geo.first.longitude() / 100.
              << ")(" << geo.second.latitude() / 100.
              << "," << geo.second.longitude() / 100.
              << ")]\n" << "Width:" << result->max.width << " Height: " << result->max.height
              << " Area:" << result->max.area() << " " << timer.elapsed() << std::endl);
        /// store data into file
        if(settings.testFlag(AppSettings::Flags::Store))
        {
            auto resultFile = result_dir + "/" + "LER_result";
            StoreResult().store(resultFile, result->max);
            TRACE("Large empty rectangle result stored: " << resultFile
	    	  << " " << timer.elapsed());
        }
    }

    TRACE("Finished: " << timer.elapsed());
    return EXIT_SUCCESS;
}

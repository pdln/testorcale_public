#include <iostream>
#include <regex>
#include <thread>

#include <AppSettings.h>
#include <Utils.h>

AppSettings::AppSettings(int argc, char *argv[])
    : m_operations()
    , m_threads(std::thread::hardware_concurrency())
    , m_generate(1000000000)
    , m_partition(1000000)
    , m_file()
{
    /// concat argv
    std::string data;
    for (int i = 0; i < argc; i++)
    {
        data.append(argv[i]);
        data.append(" ");
    }

    /// supported commands regexp
    const std::regex regex(R"((-)(((j)([\d]{1,2}))|((g)([\d]+))|((p)([\d]+))|((in)([\s])([\S]+))|(ler)|(mar)|(store)))");
    std::smatch match;

    /// parse data
    std::string::const_iterator iter = data.cbegin();
    while(std::regex_search(iter, data.cend(), match, regex))
    {
        if(match[5] != "")
        {
            m_operations = m_operations|Flags::Threads;
            std::stringstream sstream(match[5]);
            sstream >> m_threads;
            m_threads = Utils::clamp(m_threads, static_cast<uint>(1), std::thread::hardware_concurrency());
        }
        else if(match[8] != "")
        {
            m_operations = m_operations|Flags::Generate;
            std::stringstream sstream(match[8]);
            sstream >> m_generate;
        }
        else if(match[11] != "")
        {
            m_operations = m_operations|Flags::Partition;
            std::stringstream sstream(match[11]);
            sstream >> m_partition;
        }
        else if(match[15] != "")
        {
            m_operations = m_operations|Flags::InputFile;
            m_file = match[15];
        }
        else if(match[16] != "")
            m_operations = m_operations|Flags::LerAlgorithm;
        else if(match[17] != "")
            m_operations = m_operations|Flags::MarAlgorithm;
        else if(match[18] != "")
            m_operations = m_operations|Flags::Store;

        iter = match[0].second;
    }

    /// supported operation in generate data mode
    /// 1110111
    const unsigned char generateMask{0x77};
    /// supported operation in input file mode
    /// 1111001
    const unsigned char fileMask{0x79};

    /// validate and set up executable operations
    if(testFlag(Flags::InputFile))
        m_operations = static_cast<Flags>(static_cast<unsigned char>(m_operations) & fileMask);
    else
        m_operations = static_cast<Flags>(static_cast<unsigned char>(m_operations) & generateMask);
}

AppSettings::Flags AppSettings::operations() const
{
    return m_operations;
}

bool AppSettings::testFlag(const Flags flag) const
{
    return static_cast<char>(m_operations & flag);
}

uint AppSettings::threads() const
{
    return m_threads;
}

size_t AppSettings::generate() const
{
    return m_generate;
}

size_t AppSettings::partition() const
{
    return m_partition;
}

const std::string& AppSettings::file() const
{
    return m_file;
}

#include <algorithm>
#include <fstream>
#include <iterator>

#include <GeoGenerator.h>
#include <GeoCoordinates.h>
#include <Log.h>
#include <Uuid.h>

std::string GeoGenerator::generate(const std::string& dir, size_t quantity, size_t partitionSize)
{
    /// generate filename
    std::string path = dir + "/" + Uuid::generate_uuid_v4();
    /// calculate iteration count
    size_t iteration = quantity / partitionSize;
    /// calculte remainder of division
    size_t remainder = quantity % partitionSize;

    std::vector<GeoCoordinates> vector;

    /// try to reserve storage for our data
    try
    {
        /// if we have a few partition, we need to reserve partition size
        if(iteration > 0)
            vector.resize(partitionSize);
        else
            vector.reserve(remainder);
    }
    catch(const std::bad_alloc& ba)
    {
        ERROR("Error allocate memory for vector. Partition size:" << partitionSize << ". Try to change partition size.");
    }

    std::ofstream os(path, std::ios::out | std::ios::binary);

    auto writeVectorToFile = [this](std::vector<GeoCoordinates>& vector, std::ofstream& os){
        for(auto& point: vector)
            point = m_geoRandomizer.generate();

        std::sort(std::begin(vector), std::end(vector), [](const GeoCoordinates& point1, const GeoCoordinates& point2){
            return point1.longitude() < point2.longitude();
        });

        std::copy(vector.begin(), vector.end(), std::ostream_iterator<GeoCoordinates>(os));
    };

    while(iteration > 0)
    {
        writeVectorToFile(vector, os);
        --iteration;
    }

    vector.resize(remainder);
    writeVectorToFile(vector, os);

    return path;
}

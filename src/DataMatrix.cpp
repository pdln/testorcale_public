#include <DataMatrix.h>
#include <GeoRandomizer.h>
#include <GeoCoordinates.h>
#include <Matrix.h>

DataMatrix::DataMatrix()
    : m_data()
{
    const auto rows = GeoRandomizer::max_latitude + abs(GeoRandomizer::min_latitude) + 1;
    const auto cols = GeoRandomizer::max_longitude + abs(GeoRandomizer::min_longitude) + 1;
    m_data = std::make_shared<UCharMatrix>(rows, cols);
}

DataMatrix::DataPtr DataMatrix::data() const
{
    return m_data;
}

void DataMatrix::generate(const size_t quantitiy)
{
    GeoRandomizer rand;
    for(size_t i = 0; i < quantitiy; ++i)
    {
        auto point = rand.generate();
        (*m_data)[point.latitude()+GeoRandomizer::max_latitude][point.longitude()+GeoRandomizer::max_longitude] = 1;
    }
}

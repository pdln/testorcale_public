#include <fstream>
#include <iomanip>

#include <GeoRandomizer.h>
#include <Matrix.h>
#include <RectangleMapper.h>
#include <StoreResult.h>

void StoreResult::store(const std::string& file, const Rectangle<short>& rect)
{
    const auto geo = RectangleMapper<short>(rect).toGeoCoordinates();
    std::ofstream os(file, std::ios::out | std::ios::binary);
    os.setf(std::ios::fixed);
    os << std::setprecision(2);
    os << "[(" << geo.first.latitude() / 100.
       << "," << geo.first.longitude() / 100.
       << ")(" << geo.second.latitude() / 100.
       << "," << geo.second.longitude() / 100.
       << ")]\n" << "Width:" << rect.width << " Height: " << rect.height
       << " Area:" << rect.area() << "\n";
}

void StoreResult::store(const std::string& file, const DataMatrix::DataPtr data)
{
    std::ofstream os(file, std::ios::out | std::ios::binary);
    os.setf(std::ios::fixed);
    os << std::setprecision(2);
    for(size_t row = 0; row < data->rows(); ++row)
        for(size_t col = 0; col < data->cols(); ++col)
            if((*data)[row][col] == 1)
                os << "(" << (static_cast<short>(row) - GeoRandomizer::max_latitude) / 100.
                   << "," << (static_cast<short>(col) - GeoRandomizer::max_longitude) / 100.
                   << ")\n";
}

#include <memory>

#include <DataGenerator.h>
#include <FilesMerge.h>
#include <GeoGenerator.h>
#include <ThreadPool.h>

DataGenerator::DataGenerator(ThreadPool& pool, const std::string& dir)
    : m_timer()
    , m_dir(dir)
    , m_pool(pool)
    , m_results()
    , m_futures()
{
    ///
}

std::string DataGenerator::exec(size_t quantity, size_t partitionSize)
{
    auto generator = std::make_shared<GeoGenerator>();
    while(quantity > partitionSize)
    {
        m_futures.emplace(m_pool.enqueue(std::bind(&GeoGenerator::generate, generator,
                                                   m_dir,
                                                   partitionSize,
                                                   partitionSize)));
        quantity -= partitionSize;
    }

    m_futures.emplace(m_pool.enqueue(std::bind(&GeoGenerator::generate, generator,
                                               m_dir,
                                               quantity,
                                               partitionSize)));
    return waitingData();
}

std::string DataGenerator::waitingData()
{
    while(!m_futures.empty())
    {
        try
        {
            m_futures.front().wait();
            m_results.emplace(m_futures.front().get());
            m_futures.pop();
        }
        catch(const std::future_error& e)
        {
            ERROR(e.what());
        }
    }
    TRACE("Merge and sort data. Left: "
          << m_results.size()
          << " files. Time spent:"
          << m_timer.elapsed())
    return makeMerge();
}

std::string DataGenerator::makeMerge()
{
    auto merge = std::make_shared<FilesMerge>();
    if(m_results.size() > 1)
    {
        // get 2 files for merge
        while(m_results.size() > 1)
        {
            auto second = m_results.front();
            m_results.pop();
            auto first = m_results.front();
            m_results.pop();

            m_futures.emplace(m_pool.enqueue(std::bind(&FilesMerge::merge, merge,
                                                       m_dir,
                                                       first,
                                                       second)));
        }
        return waitingData();
    }
    TRACE("Merge finished: " << m_timer.elapsed());
    if(m_results.size())
        return m_results.front();
    return std::string();
}

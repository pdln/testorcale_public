#include <limits>
#include <fstream>

#include <GeoCoordinates.h>
#include <Utils.h>

GeoCoordinates::GeoCoordinates()
    : m_latitude(0)
    , m_longitude(0)
{
    ///
}

GeoCoordinates::GeoCoordinates(short latitude, short longitude)
    : m_latitude(latitude)
    , m_longitude(longitude)
{
    ///
}

GeoCoordinates::GeoCoordinates(const GeoCoordinates& other)
    : m_latitude(other.m_latitude)
    , m_longitude(other.m_longitude)
{
    ///
}

GeoCoordinates::GeoCoordinates(GeoCoordinates&& other)
    : m_latitude(std::move(other.m_latitude))
    , m_longitude(std::move(other.m_longitude))
{
    ///
}

GeoCoordinates& GeoCoordinates::operator=(const GeoCoordinates& other)
{
    if(this != &other)
    {
        this->m_latitude = other.latitude();
        this->m_longitude = other.longitude();
    }
    return *this;
}

GeoCoordinates& GeoCoordinates::operator=(GeoCoordinates&& other)
{
    if(this != &other)
    {
        this->m_latitude = std::move(other.latitude());
        this->m_longitude = std::move(other.longitude());
    }
    return *this;
}

bool GeoCoordinates::operator==(const GeoCoordinates& other)
{
    return m_latitude == other.latitude() && m_longitude == other.longitude();
}

bool GeoCoordinates::operator<=(const GeoCoordinates& other)
{
    return m_longitude <= other.longitude();
}

std::ostream& GeoCoordinates::write(std::ostream& os) const
{
    os.write(reinterpret_cast<const char *>(&m_latitude), sizeof(m_latitude));
    os.write(reinterpret_cast<const char *>(&m_longitude), sizeof(m_longitude));
    return os;
}

std::ifstream& GeoCoordinates::read(std::ifstream& in)
{
    in.read((char *) &m_latitude, sizeof(m_latitude));
    in.read((char *) &m_longitude, sizeof(m_longitude));
    return in;
}

short GeoCoordinates::latitude() const
{
    return m_latitude;
}

short GeoCoordinates::longitude() const
{
    return m_longitude;
}

void GeoCoordinates::setLatitude(const short latitude)
{
    m_latitude = latitude;
}

void GeoCoordinates::setLongitude(const short longitude)
{
    m_longitude = longitude;
}

#include <GeoRandomizer.h>
#include <GeoCoordinates.h>
#include <Utils.h>

const short GeoRandomizer::max_latitude = 9000;
const short GeoRandomizer::min_latitude =-9000;
const short GeoRandomizer::max_longitude = 18000;
const short GeoRandomizer::min_longitude = -18000;

GeoRandomizer::GeoRandomizer()
    : m_gen()
    , m_latitudeRange(min_latitude, max_latitude)
    , m_longitudeRange(min_longitude, max_longitude)
{
    /// init randomizer
    std::random_device randDevice;
    m_gen = std::mt19937(randDevice());
}

GeoCoordinates GeoRandomizer::generate()
{
    return GeoCoordinates(m_latitudeRange(m_gen), m_longitudeRange(m_gen));
}

std::pair<short, short> GeoRandomizer::latitudeRange() const
{
    return std::make_pair(m_latitudeRange.min(), m_latitudeRange.max());
}

std::pair<short, short> GeoRandomizer::longitudeRange() const
{
    return std::make_pair(m_longitudeRange.min(), m_longitudeRange.max());
}

void GeoRandomizer::setLatitudeRange(const short min, const short max)
{
    auto first = Utils::clamp<short>(min, GeoRandomizer::min_latitude, GeoRandomizer::max_latitude);
    auto second = Utils::clamp<short>(max, GeoRandomizer::min_latitude, GeoRandomizer::max_latitude);

    m_latitudeRange = std::uniform_int_distribution<>(std::min(first, second),
                                                      std::max(first, second));
}

void GeoRandomizer::setLongitudeRange(const short min, const short max)
{
    auto first = Utils::clamp<int>(min, GeoRandomizer::min_longitude, GeoRandomizer::max_longitude);
    auto second = Utils::clamp<int>(max, GeoRandomizer::min_longitude, GeoRandomizer::max_longitude);

    m_longitudeRange = std::uniform_int_distribution<>(std::min(first, second),
                                                       std::max(first, second));
}

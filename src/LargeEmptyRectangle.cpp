#include <LargeEmptyRectangle.h>
#include <Matrix.h>

Rectangle<short> LargeEmptyRectangle::exec(SourceData<GeoCoordinates>& /*data*/)
{
    // TODO implementation
    return ShortRect();
}

LargeEmptyRectangle::ResultPtr LargeEmptyRectangle::exec(DataMatrix::DataPtr data)
{
    std::list<Rectangle<short>> x_sorted;
    ResultPtr result = std::make_shared<Result>();

    const auto updateResult = [&](const ShortRect& rect) {
        if(result->max.area() < rect.area())
            result->max = rect;

//        if(rect.x == m_cols.first)
//            m_result->left.insert(rect);

//        if(rect.x + rect.width == m_cols.second || rect.x == m_cols.second - 1)
//            m_result->right.insert(rect);

//        if(rect.y == m_rows.first || rect.y-(rect.height-1) == m_rows.first)
//            m_result->top.insert(rect);

//        if(rect.y == m_rows.second - 1)
//            m_result->bottom.insert(rect);
    };

    const auto addPoint = [&](ShortRect&& rect)
    {
        updateResult(rect);
        x_sorted.emplace_back(rect);
    };

    for(decltype(data->rows()) row = 0; row < data->rows(); ++row)
    {
        auto next = x_sorted.begin();
        auto temp = next;
        decltype(data->cols()) col = 0;
        while(col < data->cols())
        {
            temp = next;
            auto val = (*data)[row][col];
            if(val == 0)
            {
                Rectangle<short> current(col, row, 0, 1);
                do
                {
                    ++col;
                    ++current.width;
                }while(col < data->cols() && (*data)[row][col] == 0);

                bool isNeedAdd = true;
                bool already_added = false;
                bool safe_next = false;
                bool first_safe = true;


                if(temp == x_sorted.end())
                    addPoint(std::move(current));
                else
                {
                    auto sss = current.x + current.width;
                    while(temp != x_sorted.end() && temp->x < sss)
                    {
                        if((*temp).y == current.y-1)
                        {
                            //1
                            if(current.x == (*temp).x && current.width == (*temp).width)
                            {
                                ++(*temp).height;
                                (*temp).y = current.y;
                                isNeedAdd = false;
                                updateResult((*temp));
                            }
                            //2
                            else if(current.x <= (*temp).x && current.x + current.width >= (*temp).x + (*temp).width)
                            {
                                (*temp).y = current.y;
                                ++(*temp).height;
                                updateResult((*temp));
                            }
                            //3
                            else if(current.x < (*temp).x && current.x + current.width >= (*temp).x &&  current.x + current.width < (*temp).x + (*temp).width)
                            {
                                if(current.x + current.width < (*temp).x + (*temp).width && first_safe)
                                {
                                    next = temp;
                                    safe_next = true;
                                    first_safe = false;
                                }
                                auto newRect = (*temp);
                                newRect.width = current.x + current.width - (*temp).x;
                                newRect.y = current.y;
                                ++newRect.height;
                                addPoint(std::move(newRect));
                            }
                            //4
                            else if(current.x > (*temp).x && current.x <= (*temp).x + (*temp).width && current.x + current.width > (*temp).x + (*temp).width)
                            {
                                auto newRect = current;
                                newRect.width = (*temp).x + (*temp).width - current.x;
                                newRect.height = (*temp).height + 1;
                                addPoint(std::move(newRect));
                            }
                            //6
                            else if(current.x >= (*temp).x && current.x + current.width <= (*temp).x + (*temp).width)
                            {
                                if(current.x + current.width < (*temp).x + (*temp).width && first_safe)
                                {
                                    next = temp;
                                    safe_next = true;
                                    first_safe = false;
                                }
                                auto newRect = current;
                                newRect.height = (*temp).height + 1;
                                addPoint(std::move(newRect));
                                isNeedAdd = false;
                            }
                            //7
                            else if(current.x > (*temp).x && current.x + current.width < (*temp).x + (*temp).width)
                            {
                                if(first_safe)
                                {
                                    next = temp;
                                    safe_next = true;
                                    first_safe = false;
                                }
                                auto newRect = current;
                                newRect.height = (*temp).height + 1;
                                addPoint(std::move(newRect));
                                isNeedAdd = false;
                            }
                        }
                        else if((*temp).y < current.y -2)
                            temp = x_sorted.erase(temp);

                        if(!safe_next)
                            next = temp;

                        ++temp;
                    }
                    if(isNeedAdd & !already_added)
                    {
                        addPoint(std::move(current));
                        already_added = true;
                    }
                }
            }
            else
                ++col;
        }
        x_sorted.sort();
    }

    return result;
}

# README #

Using random geo coordinates (latitude, longitude) generator generate 1 bln of points and develop an algorithm that calculates with an error less than 0.1 degree:

 ̶-̶ ̶r̶e̶c̶t̶a̶n̶g̶l̶e̶ ̶o̶f̶ ̶m̶i̶n̶i̶m̶a̶l̶ ̶a̶r̶e̶a̶ ̶c̶o̶n̶t̶a̶i̶n̶i̶n̶g̶ ̶3̶0̶%̶ ̶o̶f̶ ̶a̶l̶l̶ ̶p̶o̶i̶n̶t̶s̶;̶

- rectangle of a maximum area without any points



### How do I get set up? ###

# Create folder
mkdir ../leralgorithm-build && cd ../leralgorithm-build

# Setup configuration
cmake -DCMAKE_BUILD_TYPE=Release ../testorcale_public

# Build
cmake --build .

### Run configuration

# Example
./bin/leralgorithm -g1000000000 -ler -store

# Flags
-g(quantity) - generate "quantity" geo coordinates
-ler	     - exec large empty rectangle (currently executed in a main thread)
-store	     - store results to file
